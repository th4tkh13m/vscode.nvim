package.loaded['vscode'] = nil
package.loaded['vscode.util'] = nil
package.loaded['vscode.colors'] = nil
package.loaded['vscode.theme'] = nil
package.loaded['vscode.functions'] = nil

require('vscode').setup()
