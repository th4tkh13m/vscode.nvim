-- Colorscheme name:    nord.nvim
-- Description:         Port of articicestudio's nord theme for neovim

local util = require('vscode.util')

-- Load the theme
local setup = function ()
  util.load()
end

return { setup = setup }
